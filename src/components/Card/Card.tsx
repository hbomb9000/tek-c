import * as React from "react"
import styled from "styled-components";
import { AppColor, BorderWidth, px, Spacing } from "../../utils/constants";
import { HStack, VStack } from "../Layout/Layout";
import { CardProps } from "../../utils/interface";

export type Props = CardProps;

export const Card:React.FC<Props> = (props:Props) => {
  const { name, imageUrl, artist, setName, originalType  } = props;
  return(
    <HStack>
      <CardContainer cardImage={imageUrl}>
      </CardContainer>
      <CardDetailContainer>
        <ul>
          <li>
            <MetadataHeader>Name:</MetadataHeader>
            {name}
          </li>
          <li>
            <MetadataHeader>Artist:</MetadataHeader>
            {artist}
          </li>
          <li>
            <MetadataHeader>Set Name:</MetadataHeader>
            {setName}
          </li>
          <li>
            <MetadataHeader>Original Type:</MetadataHeader>
            {originalType}
          </li>
        </ul>
        {/*<button>Add to Deck</button>*/}
      </CardDetailContainer>
    </HStack>
  )};

const CardContainer = styled.div<{cardImage:string}>`
  border-radius: 5px;
  border: ${px(BorderWidth.Heavy)} solid ${AppColor.BorderColor}; 
  background: url('${props => props.cardImage}') center;
  background-size: cover;
  height: 200px;
  min-width: 150px;
`;
const CardDetailContainer = styled(VStack)`
  width: 250px;
  ul {
    list-style-type: none;
    li {
      margin-bottom: ${px(Spacing.Small)}
    }
  }
`;
const MetadataHeader = styled.span`
  font-weight: bold;
`;
