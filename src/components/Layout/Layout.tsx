import * as React from "react";
import styled from "styled-components";
import { px } from "../../utils/constants";

export interface Props {
  children: React.ReactNode;
  gap?: number;
  minWidth?: number | string;
  variant?: "horizontal" | "vertical";
  stackRef?: (ref: HTMLDivElement) => void;
}

export const Stack: React.FC<Props> = ({
  variant = "vertical",
  stackRef,
  ...rest
}) => {
  const Component = variant === "vertical" ? VerticalStack : HorizontalStack;
  return <Component {...rest} ref={stackRef} />;
};

export const HStack = (props: any) => (
  <Stack {...props} variant={"horizontal"} />
);
export const VStack = (props: any) => <Stack {...props} variant={"vertical"} />;

const gap = ({ gap = 8 }) => px(gap);
const minWidth = ({ minWidth = "auto" }: { minWidth?: number | string }) =>
  minWidth;

const BaseStack = styled.div`
  box-sizing: content-box;
  display: flex;
  flex-grow: 1;
  flex-shrink: 1;
  flex-wrap: wrap;
  & > * {
    margin: 0;
  }
`;

const HorizontalStack = styled(BaseStack)<Props>`
  flex-direction: row;
  margin-left: -${gap};
  & > * {
    margin-left: ${gap};
    margin-bottom: ${gap};
    min-width: ${({ minWidth = "auto" }) => minWidth};
  }
`;

const VerticalStack = styled(BaseStack)<Props>`
  flex-direction: column;
  margin-top: -${gap};

  & > * {
    margin-top: ${gap};
    min-width: ${minWidth};
  }
`;
