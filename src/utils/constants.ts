export enum Spacing {
  None = 0,
  XSmall = 5,
  Small = Spacing.XSmall * 2,
  Medium = Spacing.XSmall * 3,
  Large = Spacing.XSmall * 4,
  XLarge = Spacing.XSmall *5
}

export enum BorderWidth {
  Fine = 1,
  Medium,
  Heavy
}

export enum BorderRadius {
  Normal = 3,
  Large = 6
}

export enum AppColor {
  PrimaryText = "#333333",
  Background = "#FAFAFA",
  InputBackground = "#FFFFFF",
  BorderColor = "#000000",
  HeaderBackground = "linear-gradient( 135deg, #ABDCFF 10%, #0396FF 100%)",
  HeaderTextColor = "#FFFFFF",
  PrimaryAccent = "#ff2665"
}

export const px = (value: number | string): string => {
  return `${value}px`;
}