export interface CardProps {
  name: string;
  manaCost:string;
  cmc: number;
  colors: string[];
  colorIdentity: string[];
  type: string;
  rarity: string;
  setName: string;
  text: string;
  artist: string;
  originalType: string;
  layout: string;
  imageUrl: string;
}
