import React, { useCallback, useEffect, useRef, useState } from "react";
import { HStack, VStack } from "./components/Layout/Layout";
import { CardProps } from "./utils/interface";
import { Card } from "./components/Card/Card";
import { BallBeat } from "react-pure-loaders";
import styled from "styled-components";
import { AppColor, BorderWidth, px, Spacing } from "./utils/constants";
import { debounce, throttle } from "lodash";

const HEADER_HEIGHT: number = 140;
const PAGE_SIZE:number = 20;

// TODO this is the only file which needs testing at this point. There is no logic in anything else.

const App: React.FC = () => {
  const [cards, setCards] = useState([]);
  // const [deck, addToDeck] = useState([]); // TODO used for Adding Cards to deck
  // const [filterValue, setFilterValue] = useState(""); // TODO use for filtering existing values
  const [searchValue, setSearchValue] = useState("");
  const [page, setPage] = useState(1);
  const [fetching, setFetching] = useState(false);
  const [clear, setClear] = useState(false);

  const searchRef = useRef(null);

  const fetchData = useCallback(():void => {
    const nameQuery = (searchValue !== undefined && searchValue.length > 0) ? `&name=${searchValue}` : ``;
    fetch(`https://api.magicthegathering.io/v1/cards?pageSize=${PAGE_SIZE}&page=${page}${nameQuery}`)
      .then(response => {
        return response.json();
      }).then(json => {
      setPage(prevPage => prevPage + 1);
      // Here we filter out the cards which have no imageUrl. This would be a question for design on how to deal with this.
      setCards(prevCards => prevCards.concat(json.cards.filter((card: CardProps) => card.imageUrl)));
      setFetching(false);
    });
  }, [searchValue, page]);

  useEffect(() => {
    if ((cards.length === 0 && !fetching) || (clear && !fetching)) {
      setClear(false);
      setCards([]);
      setPage(1);
      setFetching(true);
      fetchData();
    }

    const handleScroll = ():void => {
      if (fetching || window.innerHeight + document.documentElement.scrollTop !== document.documentElement.offsetHeight) return;
      setFetching(true);
      fetchData();
    };

    window.addEventListener('scroll', debounce(handleScroll, 500));

    return () => window.removeEventListener('scroll', handleScroll);
  }, [cards, page, fetching, searchValue, clear, fetchData]);

  const searchInputKeyUp = (event:React.KeyboardEvent<HTMLInputElement>): void => {
    if(event.keyCode === 13 && event.currentTarget.value.length > 0) {
      setClear(true);
      setSearchValue(event.currentTarget.value);
      throttle(fetchData, 1000, {'leading': true});
    }
  };

  return (
    <>
      <CardAppHeader>
          <h1>Magic Cards</h1>
          <SearchInput onKeyUp={searchInputKeyUp} placeholder={"Enter a card name..."} ref={searchRef}/>
          <div>(press enter to search)</div>
      </CardAppHeader>
      <AppContainer>
        <CardArea>
          {
            cards.map((card:CardProps, index: number) => {
              return <Card
                {...card}
                key={`card-${index}`}
              />
            })
          }
        </CardArea>
        {fetching &&
          <VStack>
            <BallBeat
              color={AppColor.PrimaryAccent}
              loading={fetching}
            />
            <div>Fetching more Magic...</div>
          </VStack>}
      </AppContainer>
    </>
  )
};

const CardAppHeader = styled(VStack)`
  position:fixed;
  height: ${px(HEADER_HEIGHT)};
  top: 0;
  left: 0;
  right: 0;
  border-bottom: ${px(BorderWidth.Fine)} solid ${AppColor.BorderColor};  
  background: ${AppColor.HeaderBackground};
  margin-top: ${Spacing.None};
  color: ${AppColor.HeaderTextColor};
  justify-content: center;
  align-items: center;
  & :first-child {
    margin-top: ${Spacing.None};
    margin-bottom: ${px(Spacing.Small)};
  }
`;

const SearchInput = styled.input`
  background: ${AppColor.InputBackground};
  padding: ${px(Spacing.Small)} ${px(Spacing.Medium)};
  border-radius: 10px;
  border: ${px(BorderWidth.Fine)} solid ${AppColor.BorderColor};
  width: 300px;
`;

const CardArea = styled(HStack)`
  align-items: center;
  justify-content: flex-start;
`;

const AppContainer = styled(VStack)`
  margin-top: ${px(HEADER_HEIGHT)};
  padding-top: ${px(Spacing.XLarge)};
  align-items: center;
`;

export default App;
